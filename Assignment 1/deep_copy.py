def deep_copy(obj):
    if type(obj) != list:
        return obj
    copy = []
    for e in obj:
        copy.append(deep_copy(e))
    return copy
li1 = [1, 2, [3,5], 4]
li2 = deep_copy(li1)
li2[2][0] = 7
print(li1)
print(li2)
