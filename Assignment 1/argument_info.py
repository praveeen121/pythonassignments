
def args_info(*args):
    a=[]
    for name in args:
        b={}
        elem_type=type(name)
        b['element']= name
        b['data_type']= elem_type
        if(elem_type==int or elem_type==float):
            b['type'] = "numeric"
        else :
             b['type'] = "sequential"
        
        try:
            hash(name)
            b['is_mutable'] = 'false'
        except(TypeError):
            b['is_mutable'] = 'true'
        a.append(b)
    return a
print(args_info(1, 'two', ('three',), ['four'], {'five'}))


    
